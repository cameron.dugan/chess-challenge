# Chess Coding Challenge (C#)

This is my entry for [SebLague's](https://www.youtube.com/@SebastianLague) [Chess-Challenge (C#)](https://github.com/SebLague/Chess-Challenge).

My code exists entirely within the MyBot.cs file.

I hope it does well in the final bracket! :)
